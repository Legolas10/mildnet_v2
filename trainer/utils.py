import tensorflow.keras.backend as K
from numpy import sum as np_sum
import logging


def get_layers_output_by_name(model, layer_names):
    return {v: model.get_layer(v).output for v in layer_names}


def print_trainable_counts(model, mode='log'):
    trainable_count = int(np_sum([K.count_params(p) for p in set(model.trainable_weights)]))
    non_trainable_count = int(np_sum([K.count_params(p) for p in set(model.non_trainable_weights)]))
    
    if mode == 'log':
        logging.info('Total params: {:,}'.format(trainable_count + non_trainable_count))
        logging.info('Trainable params: {:,}'.format(trainable_count))
        logging.info('Non-trainable params: {:,}'.format(non_trainable_count))
    else:
        return trainable_count, non_trainable_count
