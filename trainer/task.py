import logging
import os
from .utils import print_trainable_counts
from .models import models
from .metrics import accuracy_fn, loss_functions
from .datagen import ImageDataGenerator
from tensorflow.train import MomentumOptimizer, RMSPropOptimizer
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from tensorflow.contrib.tpu import keras_to_tpu_model, TPUDistributionStrategy
from tensorflow.contrib.cluster_resolver import TPUClusterResolver
from argparse import ArgumentParser


def main(job_dir, model_name, optimizer, batch_size, loss, train_epochs, data_path, train_csv, val_csv, lr=0.01,
         weights_path=None, is_tpu=False, hyperdash_key=None):
    logging.getLogger().setLevel(logging.INFO)

    if not os.path.exists(job_dir):
        os.mkdir(job_dir)

    logging.info("Building Model: {}".format(model_name))

    accuracy = accuracy_fn(batch_size * 3)
    loss = loss_functions.get(loss)(batch_size * 3)
    model = models.get(model_name)()
    input_shape = [int(v) for v in model.input[0].shape[:2]]
    print_trainable_counts(model, mode='log')

    train_gen = ImageDataGenerator(data_path=data_path,
                                   triplets_csv=train_csv,
                                   target_size=input_shape,
                                   batch_size=batch_size,
                                   shuffle=True,
                                   augment=True,
                                   rotation_range=30,
                                   zoom_range=0.2,
                                   shear_range=0.2,
                                   vertical_flip=False,
                                   horizontal_flip=True,
                                   height_shift_range=0.1,
                                   width_shift_range=0.1)

    val_gen = ImageDataGenerator(data_path=data_path,
                                 triplets_csv=val_csv,
                                 target_size=input_shape,
                                 batch_size=batch_size)

    if weights_path:
        model.load_weights(weights_path)

    if optimizer == "mo":
        model.compile(loss=loss, optimizer=MomentumOptimizer(lr, momentum=0.9, use_nesterov=True), metrics=[accuracy])
    elif optimizer == "rms":
        model.compile(loss=loss, optimizer=RMSPropOptimizer(lr), metrics=[accuracy])
    else:
        logging.error("Optimizer not supported")
        return

    weights_path = os.path.join(job_dir, 'weights')
    if not os.path.exists(weights_path):
        os.makedirs(weights_path)

    ckptr_path = os.path.join(weights_path, "weights-epoch-{epoch:02d}-loss-{val_loss:.2f}.h5")
    model_ckptr = ModelCheckpoint(ckptr_path, save_best_only=True, save_weights_only=True, monitor="val_loss",
                                  verbose=1)

    logs_path = os.path.join(job_dir, 'logs')
    if not os.path.exists(logs_path):
        os.makedirs(logs_path)

    tensor_board = TensorBoard(log_dir=logs_path, histogram_freq=0, write_graph=True, write_images=True)
    csv_logger = CSVLogger(os.path.join(job_dir, 'training.log'))
    callbacks_list = [model_ckptr, tensor_board, csv_logger]

    if is_tpu:
        model = keras_to_tpu_model(model,
                                   strategy=TPUDistributionStrategy(
                                       TPUClusterResolver(os.environ['KUBE_GOOGLE_CLOUD_TPU_ENDPOINTS'])))
    model.fit(train_gen,
              validation_data=val_gen,
              epochs=train_epochs,
              callbacks=callbacks_list)


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument(
        '--job_dir',
        help='location to write checkpoints and export models',
        required=True)

    parser.add_argument(
        '--data_path',
        help='local paths to training data, should contain images folder',
        required=True)

    parser.add_argument(
        '--optimizer',
        help='Optimizer',
        required=True
    )

    parser.add_argument(
        '--model_name',
        help='model name',
        required=True
    )

    parser.add_argument(
        '--weights_path',
        help='location of pretrained weights path',
        default=None)

    parser.add_argument(
        '--loss',
        help='loss function',
        required=True)

    parser.add_argument(
        '--train_csv',
        help='path to train csv',
        default='train_triplets.csv')

    parser.add_argument(
        '--val_csv',
        help='path to val csv',
        default='val_triplets.csv')

    parser.add_argument(
        '--batch_size',
        help='batch size',
        default=16,
        type=int)

    parser.add_argument(
        '--is_tpu',
        help='is tpu used',
        default=False,
        type=bool)

    parser.add_argument(
        '--train_epocs',
        help='number of epochs to train',
        default=6,
        type=int)

    parser.add_argument(
        '--lr',
        help='learning rate',
        default=0.001,
        type=float)

    parser.add_argument(
        '--hyperdash_key',
        help='Hyperdash key',
        default=None)

    args = parser.parse_args()
    arguments = args.__dict__

    main(**arguments)
