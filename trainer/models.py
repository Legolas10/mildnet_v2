from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.layers import Dropout, Flatten, Dense, Input, MaxPool2D, GlobalAveragePooling2D
from tensorflow.keras.layers import Lambda, Conv2D, concatenate, ZeroPadding2D, Layer, MaxPooling2D
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K
from .utils import get_layers_output_by_name


def mildnet_vgg16():
    vgg_model = VGG16(weights="imagenet", include_top=False, input_shape=(224, 224, 3))

    for layer in vgg_model.layers[:10]:
        layer.trainable = False

    mid_layer_names = ["block1_pool", "block2_pool", "block3_pool", "block4_pool"]
    mid_layer_outputs = get_layers_output_by_name(vgg_model, mid_layer_names)

    convnet_output = GlobalAveragePooling2D()(vgg_model.output)
    for layer_name, output in mid_layer_outputs.items():
        output = GlobalAveragePooling2D()(output)
        convnet_output = concatenate([convnet_output, output])

    convnet_output = Dense(2048, activation='relu')(convnet_output)
    convnet_output = Dropout(0.6)(convnet_output)
    convnet_output = Dense(2048, activation='relu')(convnet_output)
    convnet_output = Lambda(lambda x: K.l2_normalize(x, axis=1))(convnet_output)

    final_model = Model(inputs=vgg_model.input, outputs=convnet_output)

    return final_model


def ranknet():
    vgg_model = VGG19(weights="imagenet", include_top=False, input_shape=(224, 224, 3))
    convnet_output = GlobalAveragePooling2D()(vgg_model.output)
    convnet_output = Dense(4096, activation='relu')(convnet_output)
    convnet_output = Dropout(0.5)(convnet_output)
    convnet_output = Dense(4096, activation='relu')(convnet_output)
    convnet_output = Dropout(0.5)(convnet_output)
    convnet_output = Lambda(lambda x: K.l2_normalize(x, axis=1))(convnet_output)

    s1 = MaxPool2D(pool_size=(4, 4), strides=(4, 4), padding='valid')(vgg_model.input)
    s1 = ZeroPadding2D(padding=(4, 4), data_format=None)(s1)
    s1 = Conv2D(96, kernel_size=(8, 8), strides=(4, 4), padding='valid')(s1)
    s1 = ZeroPadding2D(padding=(2, 2), data_format=None)(s1)
    s1 = MaxPool2D(pool_size=(7, 7), strides=(4, 4), padding='valid')(s1)
    s1 = Flatten()(s1)

    s2 = MaxPool2D(pool_size=(8, 8), strides=(8, 8), padding='valid')(vgg_model.input)
    s2 = ZeroPadding2D(padding=(4, 4), data_format=None)(s2)
    s2 = Conv2D(96, kernel_size=(8, 8), strides=(4, 4), padding='valid')(s2)
    s2 = ZeroPadding2D(padding=(1, 1), data_format=None)(s2)
    s2 = MaxPool2D(pool_size=(3, 3), strides=(2, 2), padding='valid')(s2)
    s2 = Flatten()(s2)

    merge_one = concatenate([s1, s2])
    merge_one_norm = Lambda(lambda x: K.l2_normalize(x, axis=1))(merge_one)
    merge_two = concatenate([merge_one_norm, convnet_output], axis=1)
    emb = Dense(4096)(merge_two)
    l2_norm_final = Lambda(lambda x: K.l2_normalize(x, axis=1))(emb)

    final_model = Model(inputs=vgg_model.input, outputs=l2_norm_final)

    return final_model


models = {'mildnet' : mildnet_vgg16, 'ranknet' : ranknet}
